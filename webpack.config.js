const path = require('path')
const HtmlWebpackPlugin = require ('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')

module.exports = {
    mode: 'development',
    entry: {
        bundle: path.resolve(__dirname, 'src/main.js')
    },
    output: {
        path: path.resolve (__dirname, 'dist'),
        filename: '[name][contenthash].js',
        clean: true
    },

    devtool: 'source-map',
    devServer: {
        static: {
            directory: path.resolve(__dirname, 'dist')
        },
        port:9000,
        open: true,
        hot: true,
        compress: true,
        historyApiFallback: true,
        proxy: {
            '/subscribe': 'http://localhost:3000',
            '/unsubscribe': 'http://localhost:3000',
        }
    },



    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },

            {
                test: /\.js$/, 
                exclude: /node_modules/, 
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },



           



        ]
    },

    plugins: [
        new HtmlWebpackPlugin(
            {
                title: 'Webpack App',
                filename: 'index.html', 
                template: 'src/template.html',

            }
        ),


        // new CopyPlugin (
        //     {
        //         patterns:[
        //                 {
        //                     from: "src/assets/images", to: "dist"
        //                 }
                        

        //         ]
        //     }
        // )
    ]




}