/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/WebsiteComponent.js":
/*!*********************************!*\
  !*** ./src/WebsiteComponent.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "webCom": () => (/* binding */ webCom)
/* harmony export */ });
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }
function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct.bind(); } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
function webCom() {
  var WebComp = /*#__PURE__*/function (_HTMLElement) {
    _inherits(WebComp, _HTMLElement);
    var _super = _createSuper(WebComp);
    function WebComp() {
      var _this;
      _classCallCheck(this, WebComp);
      _this = _super.call(this);
      _this.innerHTML = " <section class=\"app-section app-section--image-overlay app-section--image-peak\">\n        <picture  alt=\"Logo icon picture\" class=\"app-logo\"></picture>\n    \n        <h1 class=\"app-title\">\n          Your Headline <br /> Here\n        </h1>\n        <h2 class=\"app-subtitle\">\n          Lorem ipsum dolor sit amet, consectetur <br />adipiscing elit sed do eiusmod.\n        </h2>\n      </section>";
      return _this;
    }
    return _createClass(WebComp);
  }( /*#__PURE__*/_wrapNativeSuper(HTMLElement));
  window.customElements.define('first-section', WebComp);
}

/***/ }),

/***/ "./src/email-validator.js":
/*!********************************!*\
  !*** ./src/email-validator.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "validate": () => (/* binding */ validate),
/* harmony export */   "validateAsync": () => (/* binding */ validateAsync),
/* harmony export */   "validateWithLog": () => (/* binding */ validateWithLog),
/* harmony export */   "validateWithThrow": () => (/* binding */ validateWithThrow)
/* harmony export */ });
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, defineProperty = Object.defineProperty || function (obj, key, desc) { obj[key] = desc.value; }, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return defineProperty(generator, "_invoke", { value: makeInvokeMethod(innerFn, self, context) }), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; defineProperty(this, "_invoke", { value: function value(method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; } function maybeInvokeDelegate(delegate, context) { var methodName = context.method, method = delegate.iterator[methodName]; if (undefined === method) return context.delegate = null, "throw" === methodName && delegate.iterator["return"] && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method) || "return" !== methodName && (context.method = "throw", context.arg = new TypeError("The iterator does not provide a '" + methodName + "' method")), ContinueSentinel; var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, defineProperty(Gp, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), defineProperty(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (val) { var object = Object(val), keys = []; for (var key in object) keys.push(key); return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, "catch": function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
var VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com'];
function validate(email) {
  var emailEnding = email.split('@')[1];
  return VALID_EMAIL_ENDINGS.includes(emailEnding);
}
function validateAsync(_x) {
  return _validateAsync.apply(this, arguments);
}
function _validateAsync() {
  _validateAsync = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(email) {
    return _regeneratorRuntime().wrap(function _callee$(_context) {
      while (1) switch (_context.prev = _context.next) {
        case 0:
          return _context.abrupt("return", new Promise(function (resolve, reject) {
            setTimeout(function () {
              if (validate(email)) {
                resolve(true);
              } else {
                resolve(false);
              }
            }, 1000); // simulate async delay of 1 second
          }));
        case 1:
        case "end":
          return _context.stop();
      }
    }, _callee);
  }));
  return _validateAsync.apply(this, arguments);
}
function validateWithThrow(email) {
  var emailEnding = email.split('@')[1];
  if (!VALID_EMAIL_ENDINGS.includes(emailEnding)) {
    throw new Error("Provided email is invalid");
  }
}
function validateWithLog(email) {
  var emailEnding = email.split('@')[1];
  var result = VALID_EMAIL_ENDINGS.includes(emailEnding);
  console.log("".concat(email));
  return result;
}

/***/ }),

/***/ "./src/join-us-section.js":
/*!********************************!*\
  !*** ./src/join-us-section.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "JoinModule": () => (/* binding */ JoinModule)
/* harmony export */ });
/* harmony import */ var _email_validator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./email-validator.js */ "./src/email-validator.js");

var JoinModule = function () {
  var mainContainer = document.createElement('div');
  mainContainer.classList.add('join-container');
  var h2 = document.createElement('h2');
  h2.textContent = 'Join Our Program';
  var p = document.createElement('p');
  p.textContent = ' Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae !';
  var button = document.createElement('button');
  button.classList.add('btn');
  button.textContent = 'subscribe';
  var inputEmail = document.createElement('input');
  inputEmail.placeholder = 'Email';
  inputEmail.type = 'email';
  var subscribeSection = document.createElement('section');
  subscribeSection.classList.add('sub-section');
  mainContainer.appendChild(h2);
  mainContainer.appendChild(p);
  mainContainer.appendChild(button);
  mainContainer.appendChild(inputEmail);
  mainContainer.appendChild(subscribeSection);
  var unsubscribeButton = document.createElement('button');
  unsubscribeButton.classList.add('btn');
  unsubscribeButton.textContent = 'Unsubscribe';
  unsubscribeButton.style.display = 'none';
  subscribeSection.appendChild(unsubscribeButton);
  subscribeSection.appendChild(inputEmail);
  subscribeSection.appendChild(button);
  function showMore() {
    var moreInfoEl = document.querySelector('.moreInfo');
    if (moreInfoEl) {
      moreInfoEl.style.display = 'block';
    }
  }

  // function validateEmail() {
  //   const email = inputEmail.value;
  //   const isValidEmail = validate(email);
  //   if (isValidEmail) {
  //     alert('Email is valid');
  //     console.log(true);
  //   } else {
  //     alert('Email is invalid');
  //     console.log(false);
  //   }
  // }

  function sendEmail(email) {
    var xhr = new XMLHttpRequest();
    var url = '/subscribe';
    var payload = JSON.stringify({
      email: email
    });
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    button.disabled = true;
    inputEmail.disabled = true;
    button.style.opacity = '0.5';
    inputEmail.style.opacity = '0.5';
    xhr.onreadystatechange = function () {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        button.disabled = false;
        inputEmail.disabled = false;
        button.style.opacity = '1';
        inputEmail.style.opacity = '1';
        if (xhr.status === 200) {
          var response = JSON.parse(xhr.responseText);
          if (response.success) {
            button.style.display = 'none';
            unsubscribeButton.style.display = 'block';
            inputEmail.style.display = 'none';
            console.log('SUCCESS');
            localStorage.setItem('email', email);
            localStorage.setItem('isSub', true);
          }
        } else if (xhr.status === 422) {
          var _response = JSON.parse(xhr.responseText);
          window.alert(_response.error);
        }
      }
    };
    xhr.send(payload);
  }
  function validateEmail() {
    var email = inputEmail.value;
    var isValidEmail = (0,_email_validator_js__WEBPACK_IMPORTED_MODULE_0__.validate)(email);
    if (isValidEmail) {
      sendEmail(email);
    } else {
      console.log(false);
    }
  }
  function unsubscribe() {
    button.disabled = true;
    unsubscribeButton.disabled = true;
    inputEmail.disabled = true;
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/unsubscribe');
    xhr.onload = function () {
      button.disabled = false;
      unsubscribeButton.disabled = false;
      inputEmail.disabled = false;
      button.style.opacity = '0.5';
      inputEmail.style.opacity = '0.5';
      if (xhr.status === 200) {
        alert('Unsubscribed successfully!');
        button.style.display = 'block';
        unsubscribeButton.style.display = 'none';
        inputEmail.style.display = 'block';
        inputEmail.value = '';
        localStorage.removeItem('email');
        localStorage.removeItem('isSub');
      } else {
        alert('Error while unsubscribing. Please try again later.');
      }
    };
    xhr.send();
  }
  button.addEventListener('click', showMore);
  button.addEventListener('click', validateEmail);
  unsubscribeButton.addEventListener('click', unsubscribe);
  inputEmail.addEventListener('input', function (event) {
    localStorage.setItem('email', event.target.value);
  });
  window.addEventListener('load', function () {
    var email = localStorage.getItem('email');
    if (email) {
      inputEmail.value = email;
      button.style.display = 'none';
      unsubscribeButton.style.display = 'block';
      inputEmail.style.display = 'none';
    }
  });
  return {
    init: function init() {
      return mainContainer;
    }
  };
}();

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/styles/style.css":
/*!********************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/styles/style.css ***!
  \********************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_sourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/sourceMaps.js */ "./node_modules/css-loader/dist/runtime/sourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_sourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_sourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/css-loader/dist/runtime/getUrl.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_2__);
// Imports



var ___CSS_LOADER_URL_IMPORT_0___ = new URL(/* asset import */ __webpack_require__(/*! ../assets/images/your-logo-here.png */ "./src/assets/images/your-logo-here.png"), __webpack_require__.b);
var ___CSS_LOADER_URL_IMPORT_1___ = new URL(/* asset import */ __webpack_require__(/*! ../assets/images/your-logo-footer.png */ "./src/assets/images/your-logo-footer.png"), __webpack_require__.b);
var ___CSS_LOADER_URL_IMPORT_2___ = new URL(/* asset import */ __webpack_require__(/*! ../assets/images/your-image-here.jpg */ "./src/assets/images/your-image-here.jpg"), __webpack_require__.b);
var ___CSS_LOADER_URL_IMPORT_3___ = new URL(/* asset import */ __webpack_require__(/*! ../assets/images/your-image.jpg */ "./src/assets/images/your-image.jpg"), __webpack_require__.b);
var ___CSS_LOADER_URL_IMPORT_4___ = new URL(/* asset import */ __webpack_require__(/*! ../assets/images/mainImage.png */ "./src/assets/images/mainImage.png"), __webpack_require__.b);
var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_sourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
var ___CSS_LOADER_URL_REPLACEMENT_0___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_2___default()(___CSS_LOADER_URL_IMPORT_0___);
var ___CSS_LOADER_URL_REPLACEMENT_1___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_2___default()(___CSS_LOADER_URL_IMPORT_1___);
var ___CSS_LOADER_URL_REPLACEMENT_2___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_2___default()(___CSS_LOADER_URL_IMPORT_2___);
var ___CSS_LOADER_URL_REPLACEMENT_3___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_2___default()(___CSS_LOADER_URL_IMPORT_3___);
var ___CSS_LOADER_URL_REPLACEMENT_4___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_2___default()(___CSS_LOADER_URL_IMPORT_4___);
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n/*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */\n\n\n/* Document\n   ========================================================================== */\n\n\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in iOS.\n */\n\n html {\n  line-height: 1.15;\n  /* 1 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */\n}\n\n\n/* Sections\n   ========================================================================== */\n\n\n/**\n * Remove the margin in all browsers.\n */\n\nbody {\n  margin: 0;\n}\n\n\n/**\n * Render the `main` element consistently in IE.\n */\n\nmain {\n  display: block;\n}\n\n\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\n\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0;\n}\n\n\n/* Grouping content\n   ========================================================================== */\n\n\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\n\nhr {\n  box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */\n}\n\n\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\n\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */\n}\n\n\n/* Text-level semantics\n   ========================================================================== */\n\n\n/**\n * Remove the gray background on active links in IE 10.\n */\n\na {\n  background-color: transparent;\n}\n\n\n/**\n * 1. Remove the bottom border in Chrome 57-\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\n\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  text-decoration: underline dotted;\n  /* 2 */\n}\n\n\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\n\nb,\nstrong {\n  font-weight: bolder;\n}\n\n\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\n\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */\n}\n\n\n/**\n * Add the correct font size in all browsers.\n */\n\nsmall {\n  font-size: 80%;\n}\n\n\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\n\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline;\n}\n\nsub {\n  bottom: -0.25em;\n}\n\nsup {\n  top: -0.5em;\n}\n\n\n/* Embedded content\n   ========================================================================== */\n\n\n/**\n * Remove the border on images inside links in IE 10.\n */\n\nimg {\n  border-style: none;\n}\n\n\n/* Forms\n   ========================================================================== */\n\n\n/**\n * 1. Change the font styles in all browsers.\n * 2. Remove the margin in Firefox and Safari.\n */\n\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: inherit;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */\n}\n\n\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\n\nbutton,\ninput {\n  /* 1 */\n  overflow: visible;\n}\n\n\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\n\nbutton,\nselect {\n  /* 1 */\n  text-transform: none;\n}\n\n\n/**\n * Correct the inability to style clickable types in iOS and Safari.\n */\n\nbutton,\n[type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n}\n\n\n/**\n * Remove the inner border and padding in Firefox.\n */\n\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0;\n}\n\n\n/**\n * Restore the focus styles unset by the previous rule.\n */\n\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText;\n}\n\n\n/**\n * Correct the padding in Firefox.\n */\n\nfieldset {\n  padding: 0.35em 0.75em 0.625em;\n}\n\n\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\n\nlegend {\n  box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */\n}\n\n\n/**\n * Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\n\nprogress {\n  vertical-align: baseline;\n}\n\n\n/**\n * Remove the default vertical scrollbar in IE 10+.\n */\n\ntextarea {\n  overflow: auto;\n}\n\n\n/**\n * 1. Add the correct box sizing in IE 10.\n * 2. Remove the padding in IE 10.\n */\n\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */\n}\n\n\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto;\n}\n\n\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */\n}\n\n\n/**\n * Remove the inner padding in Chrome and Safari on macOS.\n */\n\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none;\n}\n\n\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */\n}\n\n\n/* Interactive\n   ========================================================================== */\n\n\n/*\n * Add the correct display in Edge, IE 10+, and Firefox.\n */\n\ndetails {\n  display: block;\n}\n\n\n/*\n * Add the correct display in all browsers.\n */\n\nsummary {\n  display: list-item;\n}\n\n\n/* Misc\n   ========================================================================== */\n\n\n/**\n * Add the correct display in IE 10+.\n */\n\ntemplate {\n  display: none;\n}\n\n\n/**\n * Add the correct display in IE 10.\n */\n\n[hidden] {\n  display: none;\n}\n\n\n.card-1, .card-2, .card-3{\n  \n  border-radius: 10px;\n  margin: 50px;\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  text-align: center;\n  padding:  20px 50px ;\n  \n}\n\n.card-1 > img, .card-2 > img , .card-3 > img{\nwidth: 150px;\nheight: 150px;\n\n\n}\n\n\n\n.card-1:hover, .card-2:hover, .card-3:hover{\n  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.3);\n  \n}\n\n\n#avatar-container {\n  display: flex;\n  flex-direction: row;\n \n\n\n}\n\n\n\n\n\n\n.first-name, .last-name {\n  text-transform: uppercase;\n  font-weight: bold;\n  \n  \n  font-family: 'Source Sans Pro', sans-serif, Arial;\n \n\n}\n\n#avatar-container > .first-name {\n  text-transform: uppercase;\n  font-weight: bold;\n}\n\n\n\n\n\n\n\n\n\n\nbody {\n  background-color: #fff;\n  font: normal 16px 'Source Sans Pro', sans-serif, Arial;\n}\n\nh1,\nh2,\nh3,\nh4,\nh5,\nh6,\nbutton {\n  font-family: 'Oswald', sans-serif;\n}\n\nmain {\n  background-color: #f9f9f9;\n}\n\n\n\n\n#app-container {\n  max-width: 1200px;\n  margin: 0 auto;\n  position: relative;\n}\n\n.app-section {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  flex-direction: column;\n}\n\n.app-logo {\n  width: 63px;\n  height: 63px;\n  object-fit: contain;\n  background-image: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");\n  \n}\n\n.app-logo--small {\n  width: 20px;\n  height: 20px;\n  margin-right: 8px;\n  background-image: url(" + ___CSS_LOADER_URL_REPLACEMENT_1___ + ");\n}\n\n.app-title {\n  font-size: 3.25rem;\n  line-height: 3.20rem;\n  text-align: center;\n}\n\n.app-subtitle {\n  font-size: 1.50rem;\n  line-height: 1.625rem;\n  font-weight: 300;\n  text-align: center;\n  font-family: 'Source Sans Pro', sans-serif, Arial;\n}\n\n.app-section__button {\n  background-color: #55C2D8;\n  border: none;\n  color: white;\n  cursor: pointer;\n  outline: none;\n}\n\n.app-header {\n  background-color: transparent;\n  margin-left: auto;\n  margin-right: auto;\n  min-height: 64px;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  z-index: 1;\n  display: grid;\n  grid-template-columns: .5fr 1fr .5fr;\n  padding: 0 140px;\n}\n\n.app-header__logo {\n  color: white;\n  justify-self: flex-start;\n  align-self: flex-end;\n  font-size: initial;\n  letter-spacing: 2.4px;\n  text-transform: uppercase;\n  text-decoration: none;\n}\n\n.app-header__nav {\n  display: inline-block;\n}\n\n.app-header__nav-list {\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  grid-column-gap: 32px;\n  justify-items: center;\n  padding: 0;\n  margin: 55px 0 0 0;\n  list-style: none;\n}\n\n.app-header__nav-list-item {\n  text-transform: uppercase;\n  font-size: 12px;\n  letter-spacing: 2.4px;\n}\n\n.app-header__nav-list-item a:hover {\n  text-decoration: underline;\n}\n\n.app-header__nav-list-item a {\n  color: white;\n  text-decoration: none;\n}\n\n.app-header__nav-menu-button {\n  display: none;\n  background-color: transparent;\n  border: 1px solid transparent;\n  border-radius: 3px;\n  box-sizing: content-box;\n  color: white;\n  cursor: pointer;\n  font-size: 0;\n  transition: all 0.25s ease-out;\n  outline: none;\n  padding: 6px;\n  position: relative;\n  user-select: none;\n}\n\n.app-header__nav-menu-button-icon {\n  width: 28px;\n  height: 18px;\n  position: relative;\n  display: inline-block;\n  font-size: 0;\n}\n\n.app-header__nav-menu-button-icon span {\n  background-color: white;\n  position: absolute;\n  border-radius: 2px;\n  transition: transform .5s ease-in-out;\n  width: 100%;\n  height: 2px;\n  left: 0;\n  transform: rotate(0);\n}\n\n.app-header__nav-menu-button-icon span:nth-child(1) {\n  top: 0;\n}\n\n.app-header__nav-menu-button-icon span:nth-child(2) {\n  top: 8px;\n  visibility: visible;\n}\n\n.app-header__nav-menu-button-icon span:nth-child(3) {\n  bottom: 0;\n}\n\n.app-section--image-peak {\n  background: linear-gradient(to top, rgba(65, 65, 65, .5), rgba(65, 65, 65, .5)), url(" + ___CSS_LOADER_URL_REPLACEMENT_2___ + ") no-repeat center/cover;\n  height: 800px;\n  max-width: 100%;\n  color: white;\n}\n\n.app-section__article {\n  display: grid;\n  grid-template-columns: 1fr 1fr;\n  grid-column-gap: 90px;\n  margin: 0 140px;\n}\n\n.app-section__button--read-more {\n  border-radius: 26px;\n  font-size: 14px;\n  height: 46px;\n  letter-spacing: 1.2px;\n  margin: 60px auto 108px;\n  text-transform: uppercase;\n  width: 135px;\n}\n\n.app-section--image-culture {\n  background: linear-gradient(to top, rgba(65, 65, 65, .7), rgba(65, 65, 65, .7)), url(" + ___CSS_LOADER_URL_REPLACEMENT_3___ + ") no-repeat center/cover;\n  height: 698px;\n  max-width: 100%;\n  color: white;\n}\n\n.app-section__button--our-culture {\n  width: 150px;\n  height: 150px;\n  border-radius: 50%;\n  margin: 64px auto;\n  padding: 0;\n  font-size: 4rem;\n}\n\n.app-footer {\n  background-color: #3b3d40;\n  color: white;\n  display: grid;\n  grid-template-columns: repeat(7, 1fr);\n  grid-template-rows: repeat(2, 1fr);\n  gap: 0 0;\n  grid-template-areas: \"logo logo address address . . .\" \". . email email rights rights rights\";\n  align-items: flex-start;\n  padding: 42px 140px 0;\n  min-height: 148px;\n}\n\n.app-footer__logo {\n  grid-area: logo;\n  display: flex;\n  align-items: center;\n  text-transform: uppercase;\n}\n\n.app-footer__address {\n  grid-area: address;\n  color: rgba(255, 255, 255, 0.3);\n}\n\n.app-footer__email {\n  grid-area: email;\n  color: rgba(255, 255, 255, 0.3);\n  text-decoration: none;\n}\n\n.app-footer__rights {\n  grid-area: rights;\n  justify-self: flex-end;\n  color: rgba(255, 255, 255, 0.3);\n  letter-spacing: 0.75px;\n  margin: 0;\n}\n\n\n\n.big-com {\n  width: 200px;\n  margin: 0;\n  padding: 0;\n  width: 100%;\n}\n\n\n\n\n.big-com h2 {\n  padding: 0;\n  margin: 0;\n  color: #000000;\n  font-size: 3rem;\n  font-family: 'Oswald', sans-serif;\n  text-align:center ;\n  padding-top: 70px;\n}\n\n\n\n\n.big-com p {\n  color:rgba(52, 52, 52, 0.795);\n  font-size: 1.50rem;\n  line-height: 1.625rem;\n  font-weight: 300;\n  text-align: center;\n  font-family: 'Source Sans Pro', sans-serif, Arial;\n  margin: 50px 80px 0 80px;\n}\n\n\n\n\n#avatar-container {\n  display: flex;\n  margin: 50px 200px 50px 200px; \n  justify-content:center;\n  \n  \n\n} \n\n#avatar-container > img {\n  padding:50px;\n}\n\n\n\n.join-container {\n\n  background: linear-gradient(to top, rgba(63, 43, 23, 0.7), rgba(54, 53, 53, 0.7)), url(" + ___CSS_LOADER_URL_REPLACEMENT_4___ + ") no-repeat center/cover;\n  height: 450px;\n  max-width: 100%;\n  color: white;\n\n  \n\n}\n\n\n.join-container h2 {\n  padding: 0;\n  margin: 0;\n  color: #f9f9f9;\n  font-size: 3rem;\n  font-family: 'Oswald', sans-serif;\n  text-align:center ;\n  padding-top: 70px;\n}\n\n\n.join-container > p {\n  color:rgba(238, 238, 238, 0.795);\n  font-size: 1.50rem;\n  line-height: 1.625rem;\n  font-weight: 300;\n  text-align: center;\n  font-family: 'Source Sans Pro', sans-serif, Arial;\n  margin: 50px 80px 0 80px;\n  \n}\n\n\n.join-container input {\n  all: unset;\n  background: rgba(255, 255, 255, 0.15);\n  width: 350px;\n  height: 36px;\n  color: #fff;\n\n  place-self: center;\n \n}\n\n\n\n.join-container button {\nbackground:#55C2D8;\ncolor: #fff;\nborder-style: none;\nborder-radius: 26px;\nfont-size: 14px;\nheight: 36px;\nletter-spacing: 1.2px;\n\ntext-transform: uppercase;\nwidth: 135px;\nplace-self: center\n\n}\n\n\n.sub-section {\n  height: 200px;\n  max-width: 800px;\n  display: grid;\n  grid-template-columns: 1fr 1fr;\n  grid-column-gap: 10px;\n  padding: 0 20% 0 20% ;\n  \n\n  \n}\n \n::placeholder {\n  color: white;\n}\n\n\n\n\n\n\n\n\n@media only screen and (max-width: 768px) {\n  \n  \n\n\n  .card-1, .card-2 {\n    display: none;\n  }\n\n  \n  \n  .sub-section {\n    grid-template-columns: 1fr;\n    padding: 0 10% 0 10% \n\n    \n   \n   \n  }\n  \n  \n  .join-container > p {\n   \n    margin: 50px 30px 0 30px;\n    \n  }\n  \n\n\n\n\n\n  \n  .app-header {\n    grid-template-columns: 1fr 1fr;\n    padding: 0 32px;\n  }\n  .app-header__nav-menu-button {\n    display: inline-block;\n    justify-self: flex-end;\n    align-self: flex-end;\n  }\n  .app-header__nav {\n    display: none;\n  }\n  .app-section--image-peak {\n    height: 648px;\n  }\n  .app-section--image-culture {\n    height: 698px;\n  }\n  .app-section__button--our-culture {\n    width: 70px;\n    height: 70px;\n    margin: 32px auto;\n    font-size: 2rem;\n  }\n  .app-section__article {\n    grid-template-columns: 1fr;\n    gap: 0 0;\n    margin: 0 16px;\n  }\n  .app-section__button--read-more {\n    margin: 24px auto 48px;\n  }\n  .app-footer {\n    grid-template-columns: 1fr;\n    grid-template-rows: repeat(9, 1fr);\n    grid-template-areas: \".\" \"logo\" \".\" \"address\" \"address\" \"email\" \".\" \".\" \"rights\";\n    align-content: center;\n    justify-content: center;\n    padding: 0 32px 0;\n  }\n  .app-footer__rights {\n    justify-self: flex-start;\n  }\n}\n", "",{"version":3,"sources":["webpack://./src/styles/style.css"],"names":[],"mappings":";AACA,2EAA2E;;;AAG3E;+EAC+E;;;AAG/E;;;EAGE;;CAED;EACC,iBAAiB;EACjB,MAAM;EACN,8BAA8B;EAC9B,MAAM;AACR;;;AAGA;+EAC+E;;;AAG/E;;EAEE;;AAEF;EACE,SAAS;AACX;;;AAGA;;EAEE;;AAEF;EACE,cAAc;AAChB;;;AAGA;;;EAGE;;AAEF;EACE,cAAc;EACd,gBAAgB;AAClB;;;AAGA;+EAC+E;;;AAG/E;;;EAGE;;AAEF;EACE,uBAAuB;EACvB,MAAM;EACN,SAAS;EACT,MAAM;EACN,iBAAiB;EACjB,MAAM;AACR;;;AAGA;;;EAGE;;AAEF;EACE,iCAAiC;EACjC,MAAM;EACN,cAAc;EACd,MAAM;AACR;;;AAGA;+EAC+E;;;AAG/E;;EAEE;;AAEF;EACE,6BAA6B;AAC/B;;;AAGA;;;EAGE;;AAEF;EACE,mBAAmB;EACnB,MAAM;EACN,0BAA0B;EAC1B,MAAM;EACN,iCAAiC;EACjC,MAAM;AACR;;;AAGA;;EAEE;;AAEF;;EAEE,mBAAmB;AACrB;;;AAGA;;;EAGE;;AAEF;;;EAGE,iCAAiC;EACjC,MAAM;EACN,cAAc;EACd,MAAM;AACR;;;AAGA;;EAEE;;AAEF;EACE,cAAc;AAChB;;;AAGA;;;EAGE;;AAEF;;EAEE,cAAc;EACd,cAAc;EACd,kBAAkB;EAClB,wBAAwB;AAC1B;;AAEA;EACE,eAAe;AACjB;;AAEA;EACE,WAAW;AACb;;;AAGA;+EAC+E;;;AAG/E;;EAEE;;AAEF;EACE,kBAAkB;AACpB;;;AAGA;+EAC+E;;;AAG/E;;;EAGE;;AAEF;;;;;EAKE,oBAAoB;EACpB,MAAM;EACN,eAAe;EACf,MAAM;EACN,iBAAiB;EACjB,MAAM;EACN,SAAS;EACT,MAAM;AACR;;;AAGA;;;EAGE;;AAEF;;EAEE,MAAM;EACN,iBAAiB;AACnB;;;AAGA;;;EAGE;;AAEF;;EAEE,MAAM;EACN,oBAAoB;AACtB;;;AAGA;;EAEE;;AAEF;;;;EAIE,0BAA0B;AAC5B;;;AAGA;;EAEE;;AAEF;;;;EAIE,kBAAkB;EAClB,UAAU;AACZ;;;AAGA;;EAEE;;AAEF;;;;EAIE,8BAA8B;AAChC;;;AAGA;;EAEE;;AAEF;EACE,8BAA8B;AAChC;;;AAGA;;;;;EAKE;;AAEF;EACE,sBAAsB;EACtB,MAAM;EACN,cAAc;EACd,MAAM;EACN,cAAc;EACd,MAAM;EACN,eAAe;EACf,MAAM;EACN,UAAU;EACV,MAAM;EACN,mBAAmB;EACnB,MAAM;AACR;;;AAGA;;EAEE;;AAEF;EACE,wBAAwB;AAC1B;;;AAGA;;EAEE;;AAEF;EACE,cAAc;AAChB;;;AAGA;;;EAGE;;AAEF;;EAEE,sBAAsB;EACtB,MAAM;EACN,UAAU;EACV,MAAM;AACR;;;AAGA;;EAEE;;AAEF;;EAEE,YAAY;AACd;;;AAGA;;;EAGE;;AAEF;EACE,6BAA6B;EAC7B,MAAM;EACN,oBAAoB;EACpB,MAAM;AACR;;;AAGA;;EAEE;;AAEF;EACE,wBAAwB;AAC1B;;;AAGA;;;EAGE;;AAEF;EACE,0BAA0B;EAC1B,MAAM;EACN,aAAa;EACb,MAAM;AACR;;;AAGA;+EAC+E;;;AAG/E;;EAEE;;AAEF;EACE,cAAc;AAChB;;;AAGA;;EAEE;;AAEF;EACE,kBAAkB;AACpB;;;AAGA;+EAC+E;;;AAG/E;;EAEE;;AAEF;EACE,aAAa;AACf;;;AAGA;;EAEE;;AAEF;EACE,aAAa;AACf;;;AAGA;;EAEE,mBAAmB;EACnB,YAAY;EACZ,aAAa;EACb,uBAAuB;EACvB,sBAAsB;EACtB,kBAAkB;EAClB,oBAAoB;;AAEtB;;AAEA;AACA,YAAY;AACZ,aAAa;;;AAGb;;;;AAIA;EACE,2CAA2C;;AAE7C;;;AAGA;EACE,aAAa;EACb,mBAAmB;;;;AAIrB;;;;;;;AAOA;EACE,yBAAyB;EACzB,iBAAiB;;;EAGjB,iDAAiD;;;AAGnD;;AAEA;EACE,yBAAyB;EACzB,iBAAiB;AACnB;;;;;;;;;;;AAWA;EACE,sBAAsB;EACtB,sDAAsD;AACxD;;AAEA;;;;;;;EAOE,iCAAiC;AACnC;;AAEA;EACE,yBAAyB;AAC3B;;;;;AAKA;EACE,iBAAiB;EACjB,cAAc;EACd,kBAAkB;AACpB;;AAEA;EACE,aAAa;EACb,mBAAmB;EACnB,uBAAuB;EACvB,sBAAsB;AACxB;;AAEA;EACE,WAAW;EACX,YAAY;EACZ,mBAAmB;EACnB,yDAA0D;;AAE5D;;AAEA;EACE,WAAW;EACX,YAAY;EACZ,iBAAiB;EACjB,yDAA4D;AAC9D;;AAEA;EACE,kBAAkB;EAClB,oBAAoB;EACpB,kBAAkB;AACpB;;AAEA;EACE,kBAAkB;EAClB,qBAAqB;EACrB,gBAAgB;EAChB,kBAAkB;EAClB,iDAAiD;AACnD;;AAEA;EACE,yBAAyB;EACzB,YAAY;EACZ,YAAY;EACZ,eAAe;EACf,aAAa;AACf;;AAEA;EACE,6BAA6B;EAC7B,iBAAiB;EACjB,kBAAkB;EAClB,gBAAgB;EAChB,kBAAkB;EAClB,MAAM;EACN,OAAO;EACP,QAAQ;EACR,UAAU;EACV,aAAa;EACb,oCAAoC;EACpC,gBAAgB;AAClB;;AAEA;EACE,YAAY;EACZ,wBAAwB;EACxB,oBAAoB;EACpB,kBAAkB;EAClB,qBAAqB;EACrB,yBAAyB;EACzB,qBAAqB;AACvB;;AAEA;EACE,qBAAqB;AACvB;;AAEA;EACE,aAAa;EACb,qCAAqC;EACrC,qBAAqB;EACrB,qBAAqB;EACrB,UAAU;EACV,kBAAkB;EAClB,gBAAgB;AAClB;;AAEA;EACE,yBAAyB;EACzB,eAAe;EACf,qBAAqB;AACvB;;AAEA;EACE,0BAA0B;AAC5B;;AAEA;EACE,YAAY;EACZ,qBAAqB;AACvB;;AAEA;EACE,aAAa;EACb,6BAA6B;EAC7B,6BAA6B;EAC7B,kBAAkB;EAClB,uBAAuB;EACvB,YAAY;EACZ,eAAe;EACf,YAAY;EACZ,8BAA8B;EAC9B,aAAa;EACb,YAAY;EACZ,kBAAkB;EAClB,iBAAiB;AACnB;;AAEA;EACE,WAAW;EACX,YAAY;EACZ,kBAAkB;EAClB,qBAAqB;EACrB,YAAY;AACd;;AAEA;EACE,uBAAuB;EACvB,kBAAkB;EAClB,kBAAkB;EAClB,qCAAqC;EACrC,WAAW;EACX,WAAW;EACX,OAAO;EACP,oBAAoB;AACtB;;AAEA;EACE,MAAM;AACR;;AAEA;EACE,QAAQ;EACR,mBAAmB;AACrB;;AAEA;EACE,SAAS;AACX;;AAEA;EACE,+IAAiJ;EACjJ,aAAa;EACb,eAAe;EACf,YAAY;AACd;;AAEA;EACE,aAAa;EACb,8BAA8B;EAC9B,qBAAqB;EACrB,eAAe;AACjB;;AAEA;EACE,mBAAmB;EACnB,eAAe;EACf,YAAY;EACZ,qBAAqB;EACrB,uBAAuB;EACvB,yBAAyB;EACzB,YAAY;AACd;;AAEA;EACE,+IAA4I;EAC5I,aAAa;EACb,eAAe;EACf,YAAY;AACd;;AAEA;EACE,YAAY;EACZ,aAAa;EACb,kBAAkB;EAClB,iBAAiB;EACjB,UAAU;EACV,eAAe;AACjB;;AAEA;EACE,yBAAyB;EACzB,YAAY;EACZ,aAAa;EACb,qCAAqC;EACrC,kCAAkC;EAClC,QAAQ;EACR,6FAA6F;EAC7F,uBAAuB;EACvB,qBAAqB;EACrB,iBAAiB;AACnB;;AAEA;EACE,eAAe;EACf,aAAa;EACb,mBAAmB;EACnB,yBAAyB;AAC3B;;AAEA;EACE,kBAAkB;EAClB,+BAA+B;AACjC;;AAEA;EACE,gBAAgB;EAChB,+BAA+B;EAC/B,qBAAqB;AACvB;;AAEA;EACE,iBAAiB;EACjB,sBAAsB;EACtB,+BAA+B;EAC/B,sBAAsB;EACtB,SAAS;AACX;;;;AAIA;EACE,YAAY;EACZ,SAAS;EACT,UAAU;EACV,WAAW;AACb;;;;;AAKA;EACE,UAAU;EACV,SAAS;EACT,cAAc;EACd,eAAe;EACf,iCAAiC;EACjC,kBAAkB;EAClB,iBAAiB;AACnB;;;;;AAKA;EACE,6BAA6B;EAC7B,kBAAkB;EAClB,qBAAqB;EACrB,gBAAgB;EAChB,kBAAkB;EAClB,iDAAiD;EACjD,wBAAwB;AAC1B;;;;;AAKA;EACE,aAAa;EACb,6BAA6B;EAC7B,sBAAsB;;;;AAIxB;;AAEA;EACE,YAAY;AACd;;;;AAIA;;EAEE,iJAA6I;EAC7I,aAAa;EACb,eAAe;EACf,YAAY;;;;AAId;;;AAGA;EACE,UAAU;EACV,SAAS;EACT,cAAc;EACd,eAAe;EACf,iCAAiC;EACjC,kBAAkB;EAClB,iBAAiB;AACnB;;;AAGA;EACE,gCAAgC;EAChC,kBAAkB;EAClB,qBAAqB;EACrB,gBAAgB;EAChB,kBAAkB;EAClB,iDAAiD;EACjD,wBAAwB;;AAE1B;;;AAGA;EACE,UAAU;EACV,qCAAqC;EACrC,YAAY;EACZ,YAAY;EACZ,WAAW;;EAEX,kBAAkB;;AAEpB;;;;AAIA;AACA,kBAAkB;AAClB,WAAW;AACX,kBAAkB;AAClB,mBAAmB;AACnB,eAAe;AACf,YAAY;AACZ,qBAAqB;;AAErB,yBAAyB;AACzB,YAAY;AACZ;;AAEA;;;AAGA;EACE,aAAa;EACb,gBAAgB;EAChB,aAAa;EACb,8BAA8B;EAC9B,qBAAqB;EACrB,qBAAqB;;;;AAIvB;;AAEA;EACE,YAAY;AACd;;;;;;;;;AASA;;;;;EAKE;IACE,aAAa;EACf;;;;EAIA;IACE,0BAA0B;IAC1B;;;;;EAKF;;;EAGA;;IAEE,wBAAwB;;EAE1B;;;;;;;;EAQA;IACE,8BAA8B;IAC9B,eAAe;EACjB;EACA;IACE,qBAAqB;IACrB,sBAAsB;IACtB,oBAAoB;EACtB;EACA;IACE,aAAa;EACf;EACA;IACE,aAAa;EACf;EACA;IACE,aAAa;EACf;EACA;IACE,WAAW;IACX,YAAY;IACZ,iBAAiB;IACjB,eAAe;EACjB;EACA;IACE,0BAA0B;IAC1B,QAAQ;IACR,cAAc;EAChB;EACA;IACE,sBAAsB;EACxB;EACA;IACE,0BAA0B;IAC1B,kCAAkC;IAClC,gFAAgF;IAChF,qBAAqB;IACrB,uBAAuB;IACvB,iBAAiB;EACnB;EACA;IACE,wBAAwB;EAC1B;AACF","sourcesContent":["\n/*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */\n\n\n/* Document\n   ========================================================================== */\n\n\n/**\n * 1. Correct the line height in all browsers.\n * 2. Prevent adjustments of font size after orientation changes in iOS.\n */\n\n html {\n  line-height: 1.15;\n  /* 1 */\n  -webkit-text-size-adjust: 100%;\n  /* 2 */\n}\n\n\n/* Sections\n   ========================================================================== */\n\n\n/**\n * Remove the margin in all browsers.\n */\n\nbody {\n  margin: 0;\n}\n\n\n/**\n * Render the `main` element consistently in IE.\n */\n\nmain {\n  display: block;\n}\n\n\n/**\n * Correct the font size and margin on `h1` elements within `section` and\n * `article` contexts in Chrome, Firefox, and Safari.\n */\n\nh1 {\n  font-size: 2em;\n  margin: 0.67em 0;\n}\n\n\n/* Grouping content\n   ========================================================================== */\n\n\n/**\n * 1. Add the correct box sizing in Firefox.\n * 2. Show the overflow in Edge and IE.\n */\n\nhr {\n  box-sizing: content-box;\n  /* 1 */\n  height: 0;\n  /* 1 */\n  overflow: visible;\n  /* 2 */\n}\n\n\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\n\npre {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */\n}\n\n\n/* Text-level semantics\n   ========================================================================== */\n\n\n/**\n * Remove the gray background on active links in IE 10.\n */\n\na {\n  background-color: transparent;\n}\n\n\n/**\n * 1. Remove the bottom border in Chrome 57-\n * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.\n */\n\nabbr[title] {\n  border-bottom: none;\n  /* 1 */\n  text-decoration: underline;\n  /* 2 */\n  text-decoration: underline dotted;\n  /* 2 */\n}\n\n\n/**\n * Add the correct font weight in Chrome, Edge, and Safari.\n */\n\nb,\nstrong {\n  font-weight: bolder;\n}\n\n\n/**\n * 1. Correct the inheritance and scaling of font size in all browsers.\n * 2. Correct the odd `em` font sizing in all browsers.\n */\n\ncode,\nkbd,\nsamp {\n  font-family: monospace, monospace;\n  /* 1 */\n  font-size: 1em;\n  /* 2 */\n}\n\n\n/**\n * Add the correct font size in all browsers.\n */\n\nsmall {\n  font-size: 80%;\n}\n\n\n/**\n * Prevent `sub` and `sup` elements from affecting the line height in\n * all browsers.\n */\n\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline;\n}\n\nsub {\n  bottom: -0.25em;\n}\n\nsup {\n  top: -0.5em;\n}\n\n\n/* Embedded content\n   ========================================================================== */\n\n\n/**\n * Remove the border on images inside links in IE 10.\n */\n\nimg {\n  border-style: none;\n}\n\n\n/* Forms\n   ========================================================================== */\n\n\n/**\n * 1. Change the font styles in all browsers.\n * 2. Remove the margin in Firefox and Safari.\n */\n\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: inherit;\n  /* 1 */\n  font-size: 100%;\n  /* 1 */\n  line-height: 1.15;\n  /* 1 */\n  margin: 0;\n  /* 2 */\n}\n\n\n/**\n * Show the overflow in IE.\n * 1. Show the overflow in Edge.\n */\n\nbutton,\ninput {\n  /* 1 */\n  overflow: visible;\n}\n\n\n/**\n * Remove the inheritance of text transform in Edge, Firefox, and IE.\n * 1. Remove the inheritance of text transform in Firefox.\n */\n\nbutton,\nselect {\n  /* 1 */\n  text-transform: none;\n}\n\n\n/**\n * Correct the inability to style clickable types in iOS and Safari.\n */\n\nbutton,\n[type=\"button\"],\n[type=\"reset\"],\n[type=\"submit\"] {\n  -webkit-appearance: button;\n}\n\n\n/**\n * Remove the inner border and padding in Firefox.\n */\n\nbutton::-moz-focus-inner,\n[type=\"button\"]::-moz-focus-inner,\n[type=\"reset\"]::-moz-focus-inner,\n[type=\"submit\"]::-moz-focus-inner {\n  border-style: none;\n  padding: 0;\n}\n\n\n/**\n * Restore the focus styles unset by the previous rule.\n */\n\nbutton:-moz-focusring,\n[type=\"button\"]:-moz-focusring,\n[type=\"reset\"]:-moz-focusring,\n[type=\"submit\"]:-moz-focusring {\n  outline: 1px dotted ButtonText;\n}\n\n\n/**\n * Correct the padding in Firefox.\n */\n\nfieldset {\n  padding: 0.35em 0.75em 0.625em;\n}\n\n\n/**\n * 1. Correct the text wrapping in Edge and IE.\n * 2. Correct the color inheritance from `fieldset` elements in IE.\n * 3. Remove the padding so developers are not caught out when they zero out\n *    `fieldset` elements in all browsers.\n */\n\nlegend {\n  box-sizing: border-box;\n  /* 1 */\n  color: inherit;\n  /* 2 */\n  display: table;\n  /* 1 */\n  max-width: 100%;\n  /* 1 */\n  padding: 0;\n  /* 3 */\n  white-space: normal;\n  /* 1 */\n}\n\n\n/**\n * Add the correct vertical alignment in Chrome, Firefox, and Opera.\n */\n\nprogress {\n  vertical-align: baseline;\n}\n\n\n/**\n * Remove the default vertical scrollbar in IE 10+.\n */\n\ntextarea {\n  overflow: auto;\n}\n\n\n/**\n * 1. Add the correct box sizing in IE 10.\n * 2. Remove the padding in IE 10.\n */\n\n[type=\"checkbox\"],\n[type=\"radio\"] {\n  box-sizing: border-box;\n  /* 1 */\n  padding: 0;\n  /* 2 */\n}\n\n\n/**\n * Correct the cursor style of increment and decrement buttons in Chrome.\n */\n\n[type=\"number\"]::-webkit-inner-spin-button,\n[type=\"number\"]::-webkit-outer-spin-button {\n  height: auto;\n}\n\n\n/**\n * 1. Correct the odd appearance in Chrome and Safari.\n * 2. Correct the outline style in Safari.\n */\n\n[type=\"search\"] {\n  -webkit-appearance: textfield;\n  /* 1 */\n  outline-offset: -2px;\n  /* 2 */\n}\n\n\n/**\n * Remove the inner padding in Chrome and Safari on macOS.\n */\n\n[type=\"search\"]::-webkit-search-decoration {\n  -webkit-appearance: none;\n}\n\n\n/**\n * 1. Correct the inability to style clickable types in iOS and Safari.\n * 2. Change font properties to `inherit` in Safari.\n */\n\n::-webkit-file-upload-button {\n  -webkit-appearance: button;\n  /* 1 */\n  font: inherit;\n  /* 2 */\n}\n\n\n/* Interactive\n   ========================================================================== */\n\n\n/*\n * Add the correct display in Edge, IE 10+, and Firefox.\n */\n\ndetails {\n  display: block;\n}\n\n\n/*\n * Add the correct display in all browsers.\n */\n\nsummary {\n  display: list-item;\n}\n\n\n/* Misc\n   ========================================================================== */\n\n\n/**\n * Add the correct display in IE 10+.\n */\n\ntemplate {\n  display: none;\n}\n\n\n/**\n * Add the correct display in IE 10.\n */\n\n[hidden] {\n  display: none;\n}\n\n\n.card-1, .card-2, .card-3{\n  \n  border-radius: 10px;\n  margin: 50px;\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  text-align: center;\n  padding:  20px 50px ;\n  \n}\n\n.card-1 > img, .card-2 > img , .card-3 > img{\nwidth: 150px;\nheight: 150px;\n\n\n}\n\n\n\n.card-1:hover, .card-2:hover, .card-3:hover{\n  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.3);\n  \n}\n\n\n#avatar-container {\n  display: flex;\n  flex-direction: row;\n \n\n\n}\n\n\n\n\n\n\n.first-name, .last-name {\n  text-transform: uppercase;\n  font-weight: bold;\n  \n  \n  font-family: 'Source Sans Pro', sans-serif, Arial;\n \n\n}\n\n#avatar-container > .first-name {\n  text-transform: uppercase;\n  font-weight: bold;\n}\n\n\n\n\n\n\n\n\n\n\nbody {\n  background-color: #fff;\n  font: normal 16px 'Source Sans Pro', sans-serif, Arial;\n}\n\nh1,\nh2,\nh3,\nh4,\nh5,\nh6,\nbutton {\n  font-family: 'Oswald', sans-serif;\n}\n\nmain {\n  background-color: #f9f9f9;\n}\n\n\n\n\n#app-container {\n  max-width: 1200px;\n  margin: 0 auto;\n  position: relative;\n}\n\n.app-section {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  flex-direction: column;\n}\n\n.app-logo {\n  width: 63px;\n  height: 63px;\n  object-fit: contain;\n  background-image: url(../assets/images/your-logo-here.png);\n  \n}\n\n.app-logo--small {\n  width: 20px;\n  height: 20px;\n  margin-right: 8px;\n  background-image: url(../assets/images/your-logo-footer.png);\n}\n\n.app-title {\n  font-size: 3.25rem;\n  line-height: 3.20rem;\n  text-align: center;\n}\n\n.app-subtitle {\n  font-size: 1.50rem;\n  line-height: 1.625rem;\n  font-weight: 300;\n  text-align: center;\n  font-family: 'Source Sans Pro', sans-serif, Arial;\n}\n\n.app-section__button {\n  background-color: #55C2D8;\n  border: none;\n  color: white;\n  cursor: pointer;\n  outline: none;\n}\n\n.app-header {\n  background-color: transparent;\n  margin-left: auto;\n  margin-right: auto;\n  min-height: 64px;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  z-index: 1;\n  display: grid;\n  grid-template-columns: .5fr 1fr .5fr;\n  padding: 0 140px;\n}\n\n.app-header__logo {\n  color: white;\n  justify-self: flex-start;\n  align-self: flex-end;\n  font-size: initial;\n  letter-spacing: 2.4px;\n  text-transform: uppercase;\n  text-decoration: none;\n}\n\n.app-header__nav {\n  display: inline-block;\n}\n\n.app-header__nav-list {\n  display: grid;\n  grid-template-columns: repeat(4, 1fr);\n  grid-column-gap: 32px;\n  justify-items: center;\n  padding: 0;\n  margin: 55px 0 0 0;\n  list-style: none;\n}\n\n.app-header__nav-list-item {\n  text-transform: uppercase;\n  font-size: 12px;\n  letter-spacing: 2.4px;\n}\n\n.app-header__nav-list-item a:hover {\n  text-decoration: underline;\n}\n\n.app-header__nav-list-item a {\n  color: white;\n  text-decoration: none;\n}\n\n.app-header__nav-menu-button {\n  display: none;\n  background-color: transparent;\n  border: 1px solid transparent;\n  border-radius: 3px;\n  box-sizing: content-box;\n  color: white;\n  cursor: pointer;\n  font-size: 0;\n  transition: all 0.25s ease-out;\n  outline: none;\n  padding: 6px;\n  position: relative;\n  user-select: none;\n}\n\n.app-header__nav-menu-button-icon {\n  width: 28px;\n  height: 18px;\n  position: relative;\n  display: inline-block;\n  font-size: 0;\n}\n\n.app-header__nav-menu-button-icon span {\n  background-color: white;\n  position: absolute;\n  border-radius: 2px;\n  transition: transform .5s ease-in-out;\n  width: 100%;\n  height: 2px;\n  left: 0;\n  transform: rotate(0);\n}\n\n.app-header__nav-menu-button-icon span:nth-child(1) {\n  top: 0;\n}\n\n.app-header__nav-menu-button-icon span:nth-child(2) {\n  top: 8px;\n  visibility: visible;\n}\n\n.app-header__nav-menu-button-icon span:nth-child(3) {\n  bottom: 0;\n}\n\n.app-section--image-peak {\n  background: linear-gradient(to top, rgba(65, 65, 65, .5), rgba(65, 65, 65, .5)), url(../assets/images/your-image-here.jpg) no-repeat center/cover;\n  height: 800px;\n  max-width: 100%;\n  color: white;\n}\n\n.app-section__article {\n  display: grid;\n  grid-template-columns: 1fr 1fr;\n  grid-column-gap: 90px;\n  margin: 0 140px;\n}\n\n.app-section__button--read-more {\n  border-radius: 26px;\n  font-size: 14px;\n  height: 46px;\n  letter-spacing: 1.2px;\n  margin: 60px auto 108px;\n  text-transform: uppercase;\n  width: 135px;\n}\n\n.app-section--image-culture {\n  background: linear-gradient(to top, rgba(65, 65, 65, .7), rgba(65, 65, 65, .7)), url(../assets/images/your-image.jpg) no-repeat center/cover;\n  height: 698px;\n  max-width: 100%;\n  color: white;\n}\n\n.app-section__button--our-culture {\n  width: 150px;\n  height: 150px;\n  border-radius: 50%;\n  margin: 64px auto;\n  padding: 0;\n  font-size: 4rem;\n}\n\n.app-footer {\n  background-color: #3b3d40;\n  color: white;\n  display: grid;\n  grid-template-columns: repeat(7, 1fr);\n  grid-template-rows: repeat(2, 1fr);\n  gap: 0 0;\n  grid-template-areas: \"logo logo address address . . .\" \". . email email rights rights rights\";\n  align-items: flex-start;\n  padding: 42px 140px 0;\n  min-height: 148px;\n}\n\n.app-footer__logo {\n  grid-area: logo;\n  display: flex;\n  align-items: center;\n  text-transform: uppercase;\n}\n\n.app-footer__address {\n  grid-area: address;\n  color: rgba(255, 255, 255, 0.3);\n}\n\n.app-footer__email {\n  grid-area: email;\n  color: rgba(255, 255, 255, 0.3);\n  text-decoration: none;\n}\n\n.app-footer__rights {\n  grid-area: rights;\n  justify-self: flex-end;\n  color: rgba(255, 255, 255, 0.3);\n  letter-spacing: 0.75px;\n  margin: 0;\n}\n\n\n\n.big-com {\n  width: 200px;\n  margin: 0;\n  padding: 0;\n  width: 100%;\n}\n\n\n\n\n.big-com h2 {\n  padding: 0;\n  margin: 0;\n  color: #000000;\n  font-size: 3rem;\n  font-family: 'Oswald', sans-serif;\n  text-align:center ;\n  padding-top: 70px;\n}\n\n\n\n\n.big-com p {\n  color:rgba(52, 52, 52, 0.795);\n  font-size: 1.50rem;\n  line-height: 1.625rem;\n  font-weight: 300;\n  text-align: center;\n  font-family: 'Source Sans Pro', sans-serif, Arial;\n  margin: 50px 80px 0 80px;\n}\n\n\n\n\n#avatar-container {\n  display: flex;\n  margin: 50px 200px 50px 200px; \n  justify-content:center;\n  \n  \n\n} \n\n#avatar-container > img {\n  padding:50px;\n}\n\n\n\n.join-container {\n\n  background: linear-gradient(to top, rgba(63, 43, 23, 0.7), rgba(54, 53, 53, 0.7)), url(../assets/images/mainImage.png) no-repeat center/cover;\n  height: 450px;\n  max-width: 100%;\n  color: white;\n\n  \n\n}\n\n\n.join-container h2 {\n  padding: 0;\n  margin: 0;\n  color: #f9f9f9;\n  font-size: 3rem;\n  font-family: 'Oswald', sans-serif;\n  text-align:center ;\n  padding-top: 70px;\n}\n\n\n.join-container > p {\n  color:rgba(238, 238, 238, 0.795);\n  font-size: 1.50rem;\n  line-height: 1.625rem;\n  font-weight: 300;\n  text-align: center;\n  font-family: 'Source Sans Pro', sans-serif, Arial;\n  margin: 50px 80px 0 80px;\n  \n}\n\n\n.join-container input {\n  all: unset;\n  background: rgba(255, 255, 255, 0.15);\n  width: 350px;\n  height: 36px;\n  color: #fff;\n\n  place-self: center;\n \n}\n\n\n\n.join-container button {\nbackground:#55C2D8;\ncolor: #fff;\nborder-style: none;\nborder-radius: 26px;\nfont-size: 14px;\nheight: 36px;\nletter-spacing: 1.2px;\n\ntext-transform: uppercase;\nwidth: 135px;\nplace-self: center\n\n}\n\n\n.sub-section {\n  height: 200px;\n  max-width: 800px;\n  display: grid;\n  grid-template-columns: 1fr 1fr;\n  grid-column-gap: 10px;\n  padding: 0 20% 0 20% ;\n  \n\n  \n}\n \n::placeholder {\n  color: white;\n}\n\n\n\n\n\n\n\n\n@media only screen and (max-width: 768px) {\n  \n  \n\n\n  .card-1, .card-2 {\n    display: none;\n  }\n\n  \n  \n  .sub-section {\n    grid-template-columns: 1fr;\n    padding: 0 10% 0 10% \n\n    \n   \n   \n  }\n  \n  \n  .join-container > p {\n   \n    margin: 50px 30px 0 30px;\n    \n  }\n  \n\n\n\n\n\n  \n  .app-header {\n    grid-template-columns: 1fr 1fr;\n    padding: 0 32px;\n  }\n  .app-header__nav-menu-button {\n    display: inline-block;\n    justify-self: flex-end;\n    align-self: flex-end;\n  }\n  .app-header__nav {\n    display: none;\n  }\n  .app-section--image-peak {\n    height: 648px;\n  }\n  .app-section--image-culture {\n    height: 698px;\n  }\n  .app-section__button--our-culture {\n    width: 70px;\n    height: 70px;\n    margin: 32px auto;\n    font-size: 2rem;\n  }\n  .app-section__article {\n    grid-template-columns: 1fr;\n    gap: 0 0;\n    margin: 0 16px;\n  }\n  .app-section__button--read-more {\n    margin: 24px auto 48px;\n  }\n  .app-footer {\n    grid-template-columns: 1fr;\n    grid-template-rows: repeat(9, 1fr);\n    grid-template-areas: \".\" \"logo\" \".\" \"address\" \"address\" \"email\" \".\" \".\" \"rights\";\n    align-content: center;\n    justify-content: center;\n    padding: 0 32px 0;\n  }\n  .app-footer__rights {\n    justify-self: flex-start;\n  }\n}\n"],"sourceRoot":""}]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/***/ ((module) => {



/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
module.exports = function (cssWithMappingToString) {
  var list = [];

  // return the list of modules as css string
  list.toString = function toString() {
    return this.map(function (item) {
      var content = "";
      var needLayer = typeof item[5] !== "undefined";
      if (item[4]) {
        content += "@supports (".concat(item[4], ") {");
      }
      if (item[2]) {
        content += "@media ".concat(item[2], " {");
      }
      if (needLayer) {
        content += "@layer".concat(item[5].length > 0 ? " ".concat(item[5]) : "", " {");
      }
      content += cssWithMappingToString(item);
      if (needLayer) {
        content += "}";
      }
      if (item[2]) {
        content += "}";
      }
      if (item[4]) {
        content += "}";
      }
      return content;
    }).join("");
  };

  // import a list of modules into the list
  list.i = function i(modules, media, dedupe, supports, layer) {
    if (typeof modules === "string") {
      modules = [[null, modules, undefined]];
    }
    var alreadyImportedModules = {};
    if (dedupe) {
      for (var k = 0; k < this.length; k++) {
        var id = this[k][0];
        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }
    for (var _k = 0; _k < modules.length; _k++) {
      var item = [].concat(modules[_k]);
      if (dedupe && alreadyImportedModules[item[0]]) {
        continue;
      }
      if (typeof layer !== "undefined") {
        if (typeof item[5] === "undefined") {
          item[5] = layer;
        } else {
          item[1] = "@layer".concat(item[5].length > 0 ? " ".concat(item[5]) : "", " {").concat(item[1], "}");
          item[5] = layer;
        }
      }
      if (media) {
        if (!item[2]) {
          item[2] = media;
        } else {
          item[1] = "@media ".concat(item[2], " {").concat(item[1], "}");
          item[2] = media;
        }
      }
      if (supports) {
        if (!item[4]) {
          item[4] = "".concat(supports);
        } else {
          item[1] = "@supports (".concat(item[4], ") {").concat(item[1], "}");
          item[4] = supports;
        }
      }
      list.push(item);
    }
  };
  return list;
};

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/getUrl.js":
/*!********************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/getUrl.js ***!
  \********************************************************/
/***/ ((module) => {



module.exports = function (url, options) {
  if (!options) {
    options = {};
  }
  if (!url) {
    return url;
  }
  url = String(url.__esModule ? url.default : url);

  // If url is already wrapped in quotes, remove them
  if (/^['"].*['"]$/.test(url)) {
    url = url.slice(1, -1);
  }
  if (options.hash) {
    url += options.hash;
  }

  // Should url be wrapped?
  // See https://drafts.csswg.org/css-values-3/#urls
  if (/["'() \t\n]|(%20)/.test(url) || options.needQuotes) {
    return "\"".concat(url.replace(/"/g, '\\"').replace(/\n/g, "\\n"), "\"");
  }
  return url;
};

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/sourceMaps.js":
/*!************************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/sourceMaps.js ***!
  \************************************************************/
/***/ ((module) => {



module.exports = function (item) {
  var content = item[1];
  var cssMapping = item[3];
  if (!cssMapping) {
    return content;
  }
  if (typeof btoa === "function") {
    var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(cssMapping))));
    var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
    var sourceMapping = "/*# ".concat(data, " */");
    return [content].concat([sourceMapping]).join("\n");
  }
  return [content].join("\n");
};

/***/ }),

/***/ "./src/styles/style.css":
/*!******************************!*\
  !*** ./src/styles/style.css ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/styleDomAPI.js */ "./node_modules/style-loader/dist/runtime/styleDomAPI.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/insertBySelector.js */ "./node_modules/style-loader/dist/runtime/insertBySelector.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js */ "./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/insertStyleElement.js */ "./node_modules/style-loader/dist/runtime/insertStyleElement.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! !../../node_modules/style-loader/dist/runtime/styleTagTransform.js */ "./node_modules/style-loader/dist/runtime/styleTagTransform.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_style_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! !!../../node_modules/css-loader/dist/cjs.js!./style.css */ "./node_modules/css-loader/dist/cjs.js!./src/styles/style.css");

      
      
      
      
      
      
      
      
      

var options = {};

options.styleTagTransform = (_node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5___default());
options.setAttributes = (_node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3___default());

      options.insert = _node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2___default().bind(null, "head");
    
options.domAPI = (_node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1___default());
options.insertStyleElement = (_node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4___default());

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_style_css__WEBPACK_IMPORTED_MODULE_6__["default"], options);




       /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_style_css__WEBPACK_IMPORTED_MODULE_6__["default"] && _node_modules_css_loader_dist_cjs_js_style_css__WEBPACK_IMPORTED_MODULE_6__["default"].locals ? _node_modules_css_loader_dist_cjs_js_style_css__WEBPACK_IMPORTED_MODULE_6__["default"].locals : undefined);


/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/***/ ((module) => {



var stylesInDOM = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDOM.length; i++) {
    if (stylesInDOM[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var indexByIdentifier = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3],
      supports: item[4],
      layer: item[5]
    };

    if (indexByIdentifier !== -1) {
      stylesInDOM[indexByIdentifier].references++;
      stylesInDOM[indexByIdentifier].updater(obj);
    } else {
      var updater = addElementStyle(obj, options);
      options.byIndex = i;
      stylesInDOM.splice(i, 0, {
        identifier: identifier,
        updater: updater,
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function addElementStyle(obj, options) {
  var api = options.domAPI(options);
  api.update(obj);

  var updater = function updater(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap && newObj.supports === obj.supports && newObj.layer === obj.layer) {
        return;
      }

      api.update(obj = newObj);
    } else {
      api.remove();
    }
  };

  return updater;
}

module.exports = function (list, options) {
  options = options || {};
  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDOM[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDOM[_index].references === 0) {
        stylesInDOM[_index].updater();

        stylesInDOM.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/insertBySelector.js":
/*!********************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/insertBySelector.js ***!
  \********************************************************************/
/***/ ((module) => {



var memo = {};
/* istanbul ignore next  */

function getTarget(target) {
  if (typeof memo[target] === "undefined") {
    var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

    if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
      try {
        // This will throw an exception if access to iframe is blocked
        // due to cross-origin restrictions
        styleTarget = styleTarget.contentDocument.head;
      } catch (e) {
        // istanbul ignore next
        styleTarget = null;
      }
    }

    memo[target] = styleTarget;
  }

  return memo[target];
}
/* istanbul ignore next  */


function insertBySelector(insert, style) {
  var target = getTarget(insert);

  if (!target) {
    throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
  }

  target.appendChild(style);
}

module.exports = insertBySelector;

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/insertStyleElement.js":
/*!**********************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/insertStyleElement.js ***!
  \**********************************************************************/
/***/ ((module) => {



/* istanbul ignore next  */
function insertStyleElement(options) {
  var element = document.createElement("style");
  options.setAttributes(element, options.attributes);
  options.insert(element, options.options);
  return element;
}

module.exports = insertStyleElement;

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js ***!
  \**********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {



/* istanbul ignore next  */
function setAttributesWithoutAttributes(styleElement) {
  var nonce =  true ? __webpack_require__.nc : 0;

  if (nonce) {
    styleElement.setAttribute("nonce", nonce);
  }
}

module.exports = setAttributesWithoutAttributes;

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/styleDomAPI.js":
/*!***************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/styleDomAPI.js ***!
  \***************************************************************/
/***/ ((module) => {



/* istanbul ignore next  */
function apply(styleElement, options, obj) {
  var css = "";

  if (obj.supports) {
    css += "@supports (".concat(obj.supports, ") {");
  }

  if (obj.media) {
    css += "@media ".concat(obj.media, " {");
  }

  var needLayer = typeof obj.layer !== "undefined";

  if (needLayer) {
    css += "@layer".concat(obj.layer.length > 0 ? " ".concat(obj.layer) : "", " {");
  }

  css += obj.css;

  if (needLayer) {
    css += "}";
  }

  if (obj.media) {
    css += "}";
  }

  if (obj.supports) {
    css += "}";
  }

  var sourceMap = obj.sourceMap;

  if (sourceMap && typeof btoa !== "undefined") {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  options.styleTagTransform(css, styleElement, options.options);
}

function removeStyleElement(styleElement) {
  // istanbul ignore if
  if (styleElement.parentNode === null) {
    return false;
  }

  styleElement.parentNode.removeChild(styleElement);
}
/* istanbul ignore next  */


function domAPI(options) {
  var styleElement = options.insertStyleElement(options);
  return {
    update: function update(obj) {
      apply(styleElement, options, obj);
    },
    remove: function remove() {
      removeStyleElement(styleElement);
    }
  };
}

module.exports = domAPI;

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/styleTagTransform.js":
/*!*********************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/styleTagTransform.js ***!
  \*********************************************************************/
/***/ ((module) => {



/* istanbul ignore next  */
function styleTagTransform(css, styleElement) {
  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css;
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild);
    }

    styleElement.appendChild(document.createTextNode(css));
  }
}

module.exports = styleTagTransform;

/***/ }),

/***/ "./src/assets/images/mainImage.png":
/*!*****************************************!*\
  !*** ./src/assets/images/mainImage.png ***!
  \*****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "b5e4545e2781377b3577.png";

/***/ }),

/***/ "./src/assets/images/your-image-here.jpg":
/*!***********************************************!*\
  !*** ./src/assets/images/your-image-here.jpg ***!
  \***********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "96b0499d27f45e1d11c5.jpg";

/***/ }),

/***/ "./src/assets/images/your-image.jpg":
/*!******************************************!*\
  !*** ./src/assets/images/your-image.jpg ***!
  \******************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "c445e404adb98d4f7754.jpg";

/***/ }),

/***/ "./src/assets/images/your-logo-footer.png":
/*!************************************************!*\
  !*** ./src/assets/images/your-logo-footer.png ***!
  \************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "a2b6f7ad6f822bff64e4.png";

/***/ }),

/***/ "./src/assets/images/your-logo-here.png":
/*!**********************************************!*\
  !*** ./src/assets/images/your-logo-here.png ***!
  \**********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "5ed305f82df085e45a65.png";

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	(() => {
/******/ 		var scriptUrl;
/******/ 		if (__webpack_require__.g.importScripts) scriptUrl = __webpack_require__.g.location + "";
/******/ 		var document = __webpack_require__.g.document;
/******/ 		if (!scriptUrl && document) {
/******/ 			if (document.currentScript)
/******/ 				scriptUrl = document.currentScript.src
/******/ 			if (!scriptUrl) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				if(scripts.length) scriptUrl = scripts[scripts.length - 1].src
/******/ 			}
/******/ 		}
/******/ 		// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration
/******/ 		// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.
/******/ 		if (!scriptUrl) throw new Error("Automatic publicPath is not supported in this browser");
/******/ 		scriptUrl = scriptUrl.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/");
/******/ 		__webpack_require__.p = scriptUrl;
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		__webpack_require__.b = document.baseURI || self.location.href;
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"bundle": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		// no on chunks loaded
/******/ 		
/******/ 		// no jsonp function
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/nonce */
/******/ 	(() => {
/******/ 		__webpack_require__.nc = undefined;
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!*********************!*\
  !*** ./src/main.js ***!
  \*********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _styles_style_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./styles/style.css */ "./src/styles/style.css");
/* harmony import */ var _join_us_section_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./join-us-section.js */ "./src/join-us-section.js");
/* harmony import */ var _email_validator_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./email-validator.js */ "./src/email-validator.js");
/* harmony import */ var _WebsiteComponent__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./WebsiteComponent */ "./src/WebsiteComponent.js");




(0,_WebsiteComponent__WEBPACK_IMPORTED_MODULE_3__.webCom)();
// import { createHtmlBlock } from './big-com.js';

var SectionCreator = function () {
  function create(type) {
    switch (type) {
      case 'standard':
        return _join_us_section_js__WEBPACK_IMPORTED_MODULE_1__.JoinModule.init();
      case 'advanced':
        var advancedContainer = document.createElement('div');
        advancedContainer.classList.add('join-container');
        var _h = document.createElement('h2');
        _h.textContent = 'Join Our Advanced Program';
        var _p = document.createElement('p');
        _p.classList.add('moreInfo');
        _p.textContent = ' Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae !';
        var button = document.createElement('button');
        button.classList.add('btn');
        button.textContent = 'subscribe to advanced program';
        var inputEmail = document.createElement('input');
        inputEmail.placeholder = 'Email';
        inputEmail.type = 'email';
        var subscribeSection = document.createElement('section');
        subscribeSection.classList.add('sub-section');
        advancedContainer.appendChild(_h);
        advancedContainer.appendChild(_p);
        advancedContainer.appendChild(subscribeSection);
        var unsubscribeButton = document.createElement('button');
        unsubscribeButton.classList.add('btn');
        unsubscribeButton.textContent = 'Unsubscribe';
        unsubscribeButton.style.display = 'none';
        subscribeSection.appendChild(unsubscribeButton);
        subscribeSection.appendChild(inputEmail);
        subscribeSection.appendChild(button);
        var showMore = function showMore() {
          var moreInfoEl = document.querySelector('.moreInfo');
          if (moreInfoEl) {
            moreInfoEl.style.display = 'block';
          }
        }; // function validateEmail() {
        //   const email = inputEmail.value;
        //   const isValidEmail = validate(email);
        //   if (isValidEmail) {
        //     button.style.display = 'none';
        //     unsubscribeButton.style.display = 'block';
        //     inputEmail.style.display = 'none';
        //     localStorage.setItem('email', email);
        //   } else {
        //     console.log(false);
        //   }
        // }
        // function sendEmail(email) {
        //   const xhr = new XMLHttpRequest();
        //   const url = '/subscribe';
        //   const payload = JSON.stringify({ email: email });
        //   xhr.open('POST', url, true);
        //   xhr.setRequestHeader('Content-type', 'application/json');
        //   xhr.onreadystatechange = function() {
        //     if (xhr.readyState === XMLHttpRequest.DONE) {
        //       if (xhr.status === 200) {
        //         const response = JSON.parse(xhr.responseText);
        //         if (response.success) {
        //           button.style.display = 'none';
        //           unsubscribeButton.style.display = 'block';
        //           inputEmail.style.display = 'none';
        //           console.log('SUCCESS');
        //           localStorage.setItem('email', email);
        //           localStorage.setItem('isSub', true)
        //         }
        //       } else if (xhr.status === 422) {
        //         const response = JSON.parse(xhr.responseText);
        //         window.alert(response.error);
        //       }
        //     }
        //   };
        //   xhr.send(payload);
        // }
        // function validateEmail() {
        //   const email = inputEmail.value;
        //   const isValidEmail = validate(email);
        //   if (isValidEmail) {
        //     sendEmail(email);
        //   } else {
        //     console.log(false);
        //   }
        // }
        var sendEmail = function sendEmail(email) {
          var xhr = new XMLHttpRequest();
          var url = '/subscribe';
          var payload = JSON.stringify({
            email: email
          });
          xhr.open('POST', url, true);
          xhr.setRequestHeader('Content-type', 'application/json');
          button.disabled = true;
          inputEmail.disabled = true;
          button.style.opacity = '0.5';
          inputEmail.style.opacity = '0.5';
          xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
              button.disabled = false;
              inputEmail.disabled = false;
              button.style.opacity = '1';
              inputEmail.style.opacity = '1';
              if (xhr.status === 200) {
                var response = JSON.parse(xhr.responseText);
                if (response.success) {
                  button.style.display = 'none';
                  unsubscribeButton.style.display = 'block';
                  inputEmail.style.display = 'none';
                  console.log('SUCCESS');
                  localStorage.setItem('email', email);
                  localStorage.setItem('isSub', true);
                }
              } else if (xhr.status === 422) {
                var _response = JSON.parse(xhr.responseText);
                window.alert(_response.error);
              }
            }
          };
          xhr.send(payload);
        };
        var validateEmail = function validateEmail() {
          var email = inputEmail.value;
          var isValidEmail = (0,_email_validator_js__WEBPACK_IMPORTED_MODULE_2__.validate)(email);
          if (isValidEmail) {
            sendEmail(email);
          } else {
            console.log(false);
          }
        }; // function validateEmail() {
        //   const email = inputEmail.value;
        //   const isValidEmail = validate(email);
        //   if (isValidEmail) {
        //     const xhr = new XMLHttpRequest();
        //     const url = '/subscribe';
        //     const data = JSON.stringify({ email });
        //     xhr.open('POST', url);
        //     xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        //     xhr.send(data);
        //     xhr.onload = function() {
        //       if (xhr.status === 200) {
        //         const response = JSON.parse(xhr.responseText);
        //         if (response.success) {
        //           console.log('SUCCESS')
        //           button.style.display = 'none';
        //           unsubscribeButton.style.display = 'block';
        //           inputEmail.style.display = 'none';
        //           localStorage.setItem('email', email);
        //           localStorage.setItem('isSub', true);
        //         } else {
        //           console.log('Server error:', response.error);
        //         }
        //       } else if (xhr.status === 422) {
        //         const response = JSON.parse(xhr.responseText);
        //         window.alert(response.error);
        //       } else {
        //         console.log('Request failed. Status:', xhr.status);
        //       }
        //     };
        //   } else {
        //     console.log('Invalid email.');
        //   }
        // }
        // function unsubscribe() {
        //   button.style.display = 'block';
        //   unsubscribeButton.style.display = 'none';
        //   inputEmail.style.display = 'block';
        //   inputEmail.value = '';
        //   localStorage.removeItem('email');
        // }
        // function unsubscribe() {
        //   const xhr = new XMLHttpRequest();
        //   xhr.open('POST', '/unsubscribe');
        //   xhr.onload = function() {
        //     if (xhr.status === 200) {
        //       alert('Unsubscribed successfully!');
        //       button.style.display = 'block';
        //       unsubscribeButton.style.display = 'none';
        //       inputEmail.style.display = 'block';
        //       inputEmail.value = '';
        //       localStorage.removeItem('email');
        //       localStorage.removeItem('isSub');
        //     } else {
        //       alert('Error while unsubscribing. Please try again later.');
        //     }
        //   };
        //   xhr.send();
        // }
        var unsubscribe = function unsubscribe() {
          button.disabled = true;
          unsubscribeButton.disabled = true;
          inputEmail.disabled = true;
          var xhr = new XMLHttpRequest();
          xhr.open('POST', '/unsubscribe');
          xhr.onload = function () {
            button.disabled = false;
            unsubscribeButton.disabled = false;
            inputEmail.disabled = false;
            button.style.opacity = '0.5';
            inputEmail.style.opacity = '0.5';
            if (xhr.status === 200) {
              alert('Unsubscribed successfully!');
              button.style.display = 'block';
              unsubscribeButton.style.display = 'none';
              inputEmail.style.display = 'block';
              inputEmail.value = '';
              localStorage.removeItem('email');
              localStorage.removeItem('isSub');
            } else {
              alert('Error while unsubscribing. Please try again later.');
            }
          };
          xhr.send();
        }; // unsubscribeButton.addEventListener('click', unsubscribeEmail);        
        button.addEventListener('click', showMore);
        button.addEventListener('click', validateEmail);
        unsubscribeButton.addEventListener('click', unsubscribe);
        inputEmail.addEventListener('input', function (event) {
          localStorage.setItem('email', event.target.value);
        });
        window.addEventListener('load', function () {
          var email = localStorage.getItem('email');
          if (email) {
            inputEmail.value = email;
            button.style.display = 'none';
            unsubscribeButton.style.display = 'block';
            inputEmail.style.display = 'none';
          }
        });
        return advancedContainer;
      default:
        throw new Error("Invalid type: ".concat(type));
    }
  }
  return {
    create: create
  };
}();

// const advancedProgram = SectionCreator.create('advanced');
// document.getElementById('join-wrapper').appendChild(advancedProgram);

var standardProgram = SectionCreator.create('standard');
document.getElementById('join-wrapper').appendChild(standardProgram);

// eslint-disable-next-line no-unused-vars
var unusedVar = "this var not used";

// eslint-disable-next-line no-console
console.log("also ignored");
console.log("also ignored");
// eslint-enable-next-line no-console

var BigCom = document.createElement("div");
BigCom.classList.add('big-com');
document.getElementById('join-wrapper').appendChild(BigCom);
var h2 = document.createElement('h2');
h2.textContent = 'Big Community of People Like You';
var p = document.createElement('p');
p.textContent = ' We’re proud of our products, and we’re really excited when we get feedback from our users.';
BigCom.appendChild(h2);
BigCom.appendChild(p);

// fetch('http://localhost:3000/community')
// .then(response => response.json())
// .then(data => {
//   const avatarContainer = document.getElementById('avatar-container');
//   data.forEach(person => {

//     const img = document.createElement('img');
//     img.src = person.avatar;
//     img.alt = `${person.firstName} ${person.lastName}'s avatar`;

fetch('http://localhost:3000/community').then(function (response) {
  return response.json();
}).then(function (data) {
  var avatarContainer = document.getElementById('avatar-container');
  data.forEach(function (person, index) {
    var cardDiv = document.createElement('div');
    cardDiv.classList.add("card-".concat(index + 1));
    var img = document.createElement('img');
    img.src = person.avatar;
    img.alt = "".concat(person.firstName, " ").concat(person.lastName, "'s avatar");
    var name = document.createElement('p');
    name.innerHTML = "".concat([person.firstName]);
    name.classList.add('first-name');
    var surname = document.createElement('p');
    surname.innerHTML = "".concat([person.lastName]);
    surname.classList.add('last-name');
    var position = document.createElement('p');
    position.innerHTML = "".concat([person.position]);
    position.classList.add('position');
    cardDiv.appendChild(img);
    avatarContainer.appendChild(cardDiv);
    cardDiv.appendChild(name);
    cardDiv.appendChild(surname);
    cardDiv.appendChild(position);
  });
})["catch"](function (error) {
  return console.error(error);
});

// const advancedProgram = SectionCreator.create('advanced');
// console.log(advancedProgram);
// document.getElementById('join-wrapper').appendChild(advancedProgram);

// инициализация адвансед типа
// просто раскомментируем две нижние строки(не забываем закомментиовать верхние две строки)
// const advancedProgram = SectionCreator.create('advanced');
// document.getElementById('advanced-wrapper').appendChild(advancedProgram);
})();

/******/ })()
;
//# sourceMappingURL=bundle4895a1a55961f483527a.js.map