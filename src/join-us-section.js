import { validate } from './email-validator.js';

export const JoinModule = (function() {
  const mainContainer = document.createElement('div')
  mainContainer.classList.add('join-container')

  const h2 = document.createElement('h2')
  h2.textContent = 'Join Our Program'

  const p = document.createElement('p')
  p.textContent= ' Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae !'


  const button = document.createElement('button')
  button.classList.add('btn')
  button.textContent= 'subscribe'

  const inputEmail = document.createElement('input')
  inputEmail.placeholder = 'Email'
  inputEmail.type = 'email';

  const subscribeSection = document.createElement ('section')
  subscribeSection.classList.add ('sub-section')

  mainContainer.appendChild(h2)
  mainContainer.appendChild(p)
  mainContainer.appendChild(button)
  mainContainer.appendChild(inputEmail)
  mainContainer.appendChild(subscribeSection)

  const unsubscribeButton = document.createElement('button');
  unsubscribeButton.classList.add('btn');
  unsubscribeButton.textContent = 'Unsubscribe';
  unsubscribeButton.style.display = 'none';
  subscribeSection.appendChild(unsubscribeButton);

  subscribeSection.appendChild(inputEmail)
  subscribeSection.appendChild(button)

  function showMore() {
    const moreInfoEl = document.querySelector('.moreInfo');
    if (moreInfoEl) {
      moreInfoEl.style.display = 'block';
    }
  }

  // function validateEmail() {
  //   const email = inputEmail.value;
  //   const isValidEmail = validate(email);
  //   if (isValidEmail) {
  //     alert('Email is valid');
  //     console.log(true);
  //   } else {
  //     alert('Email is invalid');
  //     console.log(false);
  //   }
  // }

  function sendEmail(email) {
    const xhr = new XMLHttpRequest();
    const url = '/subscribe';
    const payload = JSON.stringify({ email: email });
  
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-type', 'application/json');
  
    button.disabled = true;
    inputEmail.disabled = true;
    button.style.opacity = '0.5';
    inputEmail.style.opacity = '0.5';
  
    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        button.disabled = false;
        inputEmail.disabled = false;
        button.style.opacity = '1';
        inputEmail.style.opacity = '1';
  
        if (xhr.status === 200) {
          const response = JSON.parse(xhr.responseText);
          if (response.success) {
            button.style.display = 'none';
            unsubscribeButton.style.display = 'block';
            inputEmail.style.display = 'none';
            console.log('SUCCESS');
            localStorage.setItem('email', email);
            localStorage.setItem('isSub', true)
          }
        } else if (xhr.status === 422) {
          const response = JSON.parse(xhr.responseText);
          window.alert(response.error);
        }
      }
    };
    xhr.send(payload);
  }        
  
  function validateEmail() {
    const email = inputEmail.value;
    const isValidEmail = validate(email);
    if (isValidEmail) {
      sendEmail(email);
    } else {
      console.log(false);
    }
  }

  function unsubscribe() {
         
    button.disabled = true;
    unsubscribeButton.disabled = true;
    inputEmail.disabled = true;
    
  
    const xhr = new XMLHttpRequest();
    xhr.open('POST', '/unsubscribe');
    xhr.onload = function() {
      
      button.disabled = false;
      unsubscribeButton.disabled = false;
      inputEmail.disabled = false;
      button.style.opacity = '0.5';
      inputEmail.style.opacity = '0.5';
  
      if (xhr.status === 200) {
        alert('Unsubscribed successfully!');
        button.style.display = 'block';
        unsubscribeButton.style.display = 'none';
        inputEmail.style.display = 'block';
        inputEmail.value = '';
        localStorage.removeItem('email');
        localStorage.removeItem('isSub');
      } else {
        alert('Error while unsubscribing. Please try again later.');
      }
    };
    xhr.send();
  }

  button.addEventListener('click', showMore)
  button.addEventListener('click', validateEmail);
  unsubscribeButton.addEventListener('click', unsubscribe);
  inputEmail.addEventListener('input', (event) => {
    localStorage.setItem('email', event.target.value);
  });

  window.addEventListener('load', () => {
    const email = localStorage.getItem('email');
    if (email) {
      inputEmail.value = email;
      button.style.display = 'none';
      unsubscribeButton.style.display = 'block';
      inputEmail.style.display = 'none';
    }
  });

  return {
    init: function() {
      return mainContainer;
    }
  };
})();