// import { expect } from 'chai'; 
 
// describe('first test', () => { 
//   it('should return 2', () => {
//     expect(2).to.equal(2);
//   }) 
// });


const { expect } = require('chai');
const { validate, validateWithThrow } = require('../email-validator');

const { validateAsync } = require('../email-validator');

const { validateWithLog } = require('../email-validator');


describe('validate', () => {
  it('returns true for valid Gmail addresses', () => {
    expect(validate('john.doe@gmail.com')).to.be.true;
  });

  it('returns true for valid Outlook addresses', () => {
    expect(validate('jane.doe@outlook.com')).to.be.true;
  });

  it('returns false for invalid email addresses', () => {
    expect(validate('invalid.email@domain.com')).to.be.false;
  });

  it('returns false for email addresses with invalid endings', () => {
    expect(validate('john.doe@yahoo.com')).to.be.false;
  });

  it('returns false for email addresses without an @ symbol', () => {
    expect(validate('john.doe')).to.be.false;
  });

  it('returns false for email addresses with multiple @ symbols', () => {
    expect(validate('john@doe@gmail.com')).to.be.false;
  });
});


describe('validateAsync', () => {
    it('returns true for valid email', async () => {
      const result = await validateAsync('test@gmail.com');
      expect(result).to.be.true;
    });
  
    it('returns false for invalid email', async () => {
      const result = await validateAsync('test@yahoo.com');
      expect(result).to.be.false;
    });

    

  });


describe ('validateWithThrow', ()=> {
  it ('returns true if the email contains valid ending',()=>{
    expect(validate('john.doe@gmail.com')).to.be.true;
  })
  
  
  it('returns right Error message if email is invalid', () => {
    const invalidEmail = 'invalidemail@test.com';
    expect(() => validateWithThrow(invalidEmail)).to.throw(Error, 'Provided email is invalid');
  });


  });



  const sinon = require('sinon');
  describe('validateWithLog', () => {
    it('logs the email before returning the validation result', () => {
      const email = 'example@gmail.com';
      const logStub = sinon.stub(console, 'log');
      const result = validateWithLog(email);
      expect(result).to.be.true;
      expect(logStub.calledWith(email)).to.be.true;
      logStub.restore();
    });
  });
