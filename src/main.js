import './styles/style.css';
import { JoinModule } from './join-us-section.js';
import { validate } from './email-validator.js';
import {webCom} from './WebsiteComponent'

webCom(); 
// import { createHtmlBlock } from './big-com.js';

const SectionCreator = (function() {
  function create(type) {
    switch (type) {
      case 'standard':
        return JoinModule.init();
      case 'advanced':
        let advancedContainer = document.createElement('div');

        advancedContainer.classList.add('join-container');

        const h2 = document.createElement('h2');
        h2.textContent = 'Join Our Advanced Program';

        const p = document.createElement('p');
        p.classList.add('moreInfo');

        p.textContent= ' Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae !';


        

        const button = document.createElement('button');
        button.classList.add('btn');
        button.textContent= 'subscribe to advanced program';


        const inputEmail = document.createElement('input');
        inputEmail.placeholder = 'Email';
        inputEmail.type = 'email';

        const subscribeSection = document.createElement('section');
        subscribeSection.classList.add('sub-section');

        advancedContainer.appendChild(h2);
        advancedContainer.appendChild(p);
        advancedContainer.appendChild(subscribeSection);

        const unsubscribeButton = document.createElement('button');
        unsubscribeButton.classList.add('btn');
        unsubscribeButton.textContent = 'Unsubscribe';
        unsubscribeButton.style.display = 'none';
        subscribeSection.appendChild(unsubscribeButton);

        subscribeSection.appendChild(inputEmail);
        subscribeSection.appendChild(button);

        function showMore() {
          const moreInfoEl = document.querySelector('.moreInfo');
          if (moreInfoEl) {
            moreInfoEl.style.display = 'block';
          }
        }

        // function validateEmail() {
        //   const email = inputEmail.value;
        //   const isValidEmail = validate(email);
        //   if (isValidEmail) {
        //     button.style.display = 'none';
        //     unsubscribeButton.style.display = 'block';
        //     inputEmail.style.display = 'none';
        //     localStorage.setItem('email', email);
        //   } else {
        //     console.log(false);
        //   }
        // }

        // function sendEmail(email) {
        //   const xhr = new XMLHttpRequest();
        //   const url = '/subscribe';
        //   const payload = JSON.stringify({ email: email });
        
        //   xhr.open('POST', url, true);
        //   xhr.setRequestHeader('Content-type', 'application/json');
        //   xhr.onreadystatechange = function() {
        //     if (xhr.readyState === XMLHttpRequest.DONE) {
        //       if (xhr.status === 200) {
        //         const response = JSON.parse(xhr.responseText);
        //         if (response.success) {
        //           button.style.display = 'none';
        //           unsubscribeButton.style.display = 'block';
        //           inputEmail.style.display = 'none';
        //           console.log('SUCCESS');
        //           localStorage.setItem('email', email);
        //           localStorage.setItem('isSub', true)
        //         }
        //       } else if (xhr.status === 422) {
        //         const response = JSON.parse(xhr.responseText);
        //         window.alert(response.error);
        //       }
        //     }
        //   };
        //   xhr.send(payload);
        // }

        
        
        // function validateEmail() {
        //   const email = inputEmail.value;
        //   const isValidEmail = validate(email);
        //   if (isValidEmail) {
        //     sendEmail(email);
        //   } else {
        //     console.log(false);
        //   }
        // }
        
        function sendEmail(email) {
          const xhr = new XMLHttpRequest();
          const url = '/subscribe';
          const payload = JSON.stringify({ email: email });
        
          xhr.open('POST', url, true);
          xhr.setRequestHeader('Content-type', 'application/json');
        
          button.disabled = true;
          inputEmail.disabled = true;
          button.style.opacity = '0.5';
          inputEmail.style.opacity = '0.5';
        
          xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
              button.disabled = false;
              inputEmail.disabled = false;
              button.style.opacity = '1';
              inputEmail.style.opacity = '1';
        
              if (xhr.status === 200) {
                const response = JSON.parse(xhr.responseText);
                if (response.success) {
                  button.style.display = 'none';
                  unsubscribeButton.style.display = 'block';
                  inputEmail.style.display = 'none';
                  console.log('SUCCESS');
                  localStorage.setItem('email', email);
                  localStorage.setItem('isSub', true)
                }
              } else if (xhr.status === 422) {
                const response = JSON.parse(xhr.responseText);
                window.alert(response.error);
              }
            }
          };
          xhr.send(payload);
        }        
        
        function validateEmail() {
          const email = inputEmail.value;
          const isValidEmail = validate(email);
          if (isValidEmail) {
            sendEmail(email);
          } else {
            console.log(false);
          }
        }
        

        // function validateEmail() {
        //   const email = inputEmail.value;
        //   const isValidEmail = validate(email);
          
        //   if (isValidEmail) {
        //     const xhr = new XMLHttpRequest();
        //     const url = '/subscribe';
        //     const data = JSON.stringify({ email });
            
        //     xhr.open('POST', url);
        //     xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        //     xhr.send(data);
            
        //     xhr.onload = function() {
        //       if (xhr.status === 200) {
        //         const response = JSON.parse(xhr.responseText);
        //         if (response.success) {
        //           console.log('SUCCESS')
        //           button.style.display = 'none';
        //           unsubscribeButton.style.display = 'block';
        //           inputEmail.style.display = 'none';
        //           localStorage.setItem('email', email);
        //           localStorage.setItem('isSub', true);
        //         } else {
        //           console.log('Server error:', response.error);
        //         }
        //       } else if (xhr.status === 422) {
        //         const response = JSON.parse(xhr.responseText);
        //         window.alert(response.error);
        //       } else {
        //         console.log('Request failed. Status:', xhr.status);
        //       }
        //     };
        //   } else {
        //     console.log('Invalid email.');
        //   }
        // }
        


        // function unsubscribe() {
        //   button.style.display = 'block';
        //   unsubscribeButton.style.display = 'none';
        //   inputEmail.style.display = 'block';
        //   inputEmail.value = '';
        //   localStorage.removeItem('email');
        // }

        // function unsubscribe() {
        //   const xhr = new XMLHttpRequest();
        //   xhr.open('POST', '/unsubscribe');
        //   xhr.onload = function() {
        //     if (xhr.status === 200) {
        //       alert('Unsubscribed successfully!');
        //       button.style.display = 'block';
        //       unsubscribeButton.style.display = 'none';
        //       inputEmail.style.display = 'block';
        //       inputEmail.value = '';
        //       localStorage.removeItem('email');
        //       localStorage.removeItem('isSub');
        //     } else {
        //       alert('Error while unsubscribing. Please try again later.');
        //     }
        //   };
        //   xhr.send();
        // }
        function unsubscribe() {
         
          button.disabled = true;
          unsubscribeButton.disabled = true;
          inputEmail.disabled = true;
          
        
          const xhr = new XMLHttpRequest();
          xhr.open('POST', '/unsubscribe');
          xhr.onload = function() {
            
            button.disabled = false;
            unsubscribeButton.disabled = false;
            inputEmail.disabled = false;
            button.style.opacity = '0.5';
            inputEmail.style.opacity = '0.5';
        
            if (xhr.status === 200) {
              alert('Unsubscribed successfully!');
              button.style.display = 'block';
              unsubscribeButton.style.display = 'none';
              inputEmail.style.display = 'block';
              inputEmail.value = '';
              localStorage.removeItem('email');
              localStorage.removeItem('isSub');
            } else {
              alert('Error while unsubscribing. Please try again later.');
            }
          };
          xhr.send();
        }
        
        
        

        // unsubscribeButton.addEventListener('click', unsubscribeEmail);        

        button.addEventListener('click', showMore);
        button.addEventListener('click', validateEmail);
        unsubscribeButton.addEventListener('click', unsubscribe);
        inputEmail.addEventListener('input', (event) => {
          localStorage.setItem('email', event.target.value);
        });

        window.addEventListener('load', () => {
          const email = localStorage.getItem('email');
          if (email) {
            inputEmail.value = email;
            button.style.display = 'none';
            unsubscribeButton.style.display = 'block';
            inputEmail.style.display = 'none';
          }
        });

        return advancedContainer;
      default:
        throw new Error(`Invalid type: ${type}`);
    }
  }

  return {
    create
  };
})();

// const advancedProgram = SectionCreator.create('advanced');
// document.getElementById('join-wrapper').appendChild(advancedProgram);




const standardProgram = SectionCreator.create('standard');
document.getElementById('join-wrapper').appendChild(standardProgram);

// eslint-disable-next-line no-unused-vars
const unusedVar = "this var not used";

// eslint-disable-next-line no-console
console.log("also ignored");
console.log("also ignored");
// eslint-enable-next-line no-console










  const BigCom = document.createElement("div"); 
  BigCom.classList.add ('big-com')



  document.getElementById('join-wrapper').appendChild(BigCom);  
   


  const h2 = document.createElement('h2')
  h2.textContent = 'Big Community of People Like You'

  const p = document.createElement('p')
  p.textContent= ' We’re proud of our products, and we’re really excited when we get feedback from our users.'


  BigCom.appendChild(h2)
  BigCom.appendChild(p)






  // fetch('http://localhost:3000/community')
  // .then(response => response.json())
  // .then(data => {
  //   const avatarContainer = document.getElementById('avatar-container');
  //   data.forEach(person => {
      
  //     const img = document.createElement('img');
  //     img.src = person.avatar;
  //     img.alt = `${person.firstName} ${person.lastName}'s avatar`;




  fetch('http://localhost:3000/community')
  .then(response => response.json())
  .then(data => {
    const avatarContainer = document.getElementById('avatar-container');
    data.forEach((person, index) => {
      const cardDiv = document.createElement('div');
      cardDiv.classList.add(`card-${index+1}`);


      
      const img = document.createElement('img');
      img.src = person.avatar;
      img.alt = `${person.firstName} ${person.lastName}'s avatar`;

      const name = document.createElement ('p');
      name.innerHTML = `${[person.firstName]}`;
      name.classList.add ('first-name');

      const surname = document.createElement ('p');
      surname.innerHTML = `${[person.lastName]}`;
      surname.classList.add ('last-name');

      const position = document.createElement ('p');
      position.innerHTML = `${[person.position]}`;
      position.classList.add ('position');

      



      cardDiv.appendChild(img);
      avatarContainer.appendChild(cardDiv);

      cardDiv.appendChild(name);
      cardDiv.appendChild(surname);
      cardDiv.appendChild(position);

 
  
      


      
     

      



      
      
      




      
    });
  })
  .catch(error => console.error(error));

 


























// const advancedProgram = SectionCreator.create('advanced');
// console.log(advancedProgram);
// document.getElementById('join-wrapper').appendChild(advancedProgram);



// инициализация адвансед типа
// просто раскомментируем две нижние строки(не забываем закомментиовать верхние две строки)
// const advancedProgram = SectionCreator.create('advanced');
// document.getElementById('advanced-wrapper').appendChild(advancedProgram);




