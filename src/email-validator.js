const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com'];

export function validate(email) {
  const emailEnding = email.split('@')[1];
  return VALID_EMAIL_ENDINGS.includes(emailEnding);
}





export async function validateAsync(email) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (validate(email)) {
        resolve(true);
      } else {
        resolve(false);
      }
    }, 1000); // simulate async delay of 1 second
  });
}

export function validateWithThrow(email) {
  const emailEnding = email.split('@')[1];
  if (!VALID_EMAIL_ENDINGS.includes(emailEnding)) {
    throw new Error("Provided email is invalid");
  }
}





export function validateWithLog(email) {
  const emailEnding = email.split('@')[1];
  const result = VALID_EMAIL_ENDINGS.includes(emailEnding);
  console.log(`${email}`);
  return result;
}